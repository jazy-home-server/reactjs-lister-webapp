import ResponseError from 'api/ResponseError';
import List from './list';

const userData = {
  id: 1,
  username: 'applelover',
};

async function get(username) {
  // return axios.get(`/users/${username}`).then((response) => response.data.data);
  if (username === userData.username) return userData;
  throw new ResponseError({ status: 404 }, 'User not found');
}

async function getList(userId) {
  // const payload = { params: { user_id: userId } };
  // return axios.get('/acmusic/lists', payload).then((response) => response.data.data[0]);
  if (List.listData.user_id === userId) return List.listData;
  throw new ResponseError({ status: 404 }, 'User has no list');
}

async function post(username) {
  // return axios.post('/users', { username }).then((response) => response.data.data);
  if (username === userData.username) {
    throw new ResponseError(
      {
        data: {
          errors: {
            username: [['username has already been taken']],
          },
        },
      },
      'User already exists'
    );
  }
  userData.username = username;
  userData.id += 1;
  return userData;
}

export default {
  get,
  getList,
  post,
  userData,
};
