import ResponseError from 'api/ResponseError';

const listData = {
  id: 2,
  user_id: 1,
  pin: '1234',
};

async function post(userId, pin) {
  if (listData.user_id === userId) throw new ResponseError({}, 'User already has list');
  listData.user_id = userId;
  listData.pin = pin;
  listData.id += 1;
  return listData;
}

async function auth(listId, pin) {
  if (listId !== listData.id) throw new ResponseError({ status: 404 }, 'List not found');
  if (pin !== listData.pin) throw new ResponseError({ status: 401 }, 'Unauthorized');
  return listData;
}

export default {
  auth,
  post,
  listData,
};
