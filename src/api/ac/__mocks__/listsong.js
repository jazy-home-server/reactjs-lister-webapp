import ResponseError from 'api/ResponseError';
import List from './list';

const listSongs = [
  {
    id: 1,
    list_id: 2,
    song_id: 1,
    checked: 0,
    song: {
      id: 1,
      name: 'apple1',
      img_url: 'test.example.com',
    },
  },
  {
    id: 2,
    list_id: 2,
    song_id: 2,
    checked: 0,
    song: {
      id: 2,
      name: 'apple2',
      img_url: 'test2.example.com',
    },
  },
  {
    id: 3,
    list_id: 2,
    song_id: 3,
    checked: 1,
    song: {
      id: 3,
      name: 'apple3',
      img_url: 'test3.example.com',
    },
  },
];

function processListSongData(listSong) {
  return {
    ...listSong,
    checked: listSong.checked === 1 || listSong.checked === true || listSong.checked === 'true',
  };
}

async function getFromList(listId) {
  // const payload = { params: { expand: 'song', list_id: listId, pag_count: 200 } };
  // const res = await axios.get('/acmusic/listsongs', payload);
  if (listId === listSongs[0].list_id) return listSongs.map(processListSongData);
  throw new ResponseError({ status: 404 }, 'List has no songs');
}

async function patch(id, pin, checked) {
  // const response = await axios.patch(`/acmusic/listsongs/${id}`, { pin, checked: checked ? 1 : 0 });
  const listSong = listSongs.findIndex((e) => e.id === id);
  if (listSong < 0) throw new ResponseError({ status: 404 }, 'No such listsong');
  if (List.listData.pin !== pin) throw new ResponseError({ status: 401 }, 'Unauthorized');
  listSongs[listSong].checked = checked;
  return processListSongData(listSongs[listSong]);
}

export default {
  patch,
  getFromList,
  listSongs,
};
