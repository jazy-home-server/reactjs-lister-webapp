import ResponseError from 'api/ResponseError';
import User from 'api/ac/user';
import {
  createUserWithList,
  fetchUser,
  fetchUserList,
  authListPin,
  patchListSong,
} from './acsongs';

jest.mock('api/ac/list');
jest.mock('api/ac/listsong');
jest.mock('api/ac/user');

describe('acsongs api handler', () => {
  it('should fetch a user', async () => {
    const res = await fetchUser('applelover');
    expect(res.username).toBe('applelover');
  });

  it('should return an error on user not found', async () => {
    expect.hasAssertions();
    try {
      await fetchUser('apple1');
    } catch (err) {
      expect(err.name).toBe('ResponseError');
      expect(err instanceof ResponseError).toBeTruthy();
    }
  });

  it('should fetch a users list', async () => {
    const res = await fetchUserList(1);
    expect(res.user_id).toBe(1);
  });

  it('should fail to fetch a users list', async () => {
    expect.hasAssertions();
    try {
      await fetchUserList(2);
    } catch (err) {
      expect(err.name).toBe('ResponseError');
      expect(err instanceof ResponseError).toBeTruthy();
      expect(err.response.status).toBe(404);
    }
  });

  it('should auth a list', async () => {
    const res = await authListPin(2, '1234');
    expect(res.id).toBe(2);
  });

  it('should fail to auth a list', async () => {
    expect.hasAssertions();
    try {
      await authListPin(2, '1235');
    } catch (error) {
      expect(error.name).toBe('ResponseError');
      expect(error instanceof ResponseError).toBeTruthy();
      expect(error.response.status).toBe(401);
    }
  });

  it('should fail to auth a list not found', async () => {
    expect.hasAssertions();
    try {
      await authListPin(1, '1234');
    } catch (error) {
      expect(error.name).toBe('ResponseError');
      expect(error instanceof ResponseError).toBeTruthy();
      expect(error.response.status).toBe(404);
    }
  });

  it('should patch a listsong', async () => {
    const res = await patchListSong(2, '1234', true);
    expect(res.id).toBe(2);
    expect(res.checked).toBe(true);
  });

  it('should fail to patch a listsong with incorrect pin', async () => {
    expect.hasAssertions();
    try {
      await patchListSong(2, '1235', true);
    } catch (error) {
      expect(error.name).toBe('ResponseError');
      expect(error instanceof ResponseError).toBeTruthy();
      expect(error.response.status).toBe(401);
    }
  });

  it('should fail to patch a listsong not found', async () => {
    expect.hasAssertions();
    try {
      await patchListSong(5, '1234', true);
    } catch (error) {
      expect(error.name).toBe('ResponseError');
      expect(error instanceof ResponseError).toBeTruthy();
      expect(error.response.status).toBe(404);
    }
  });

  it('should create a user with list', async () => {
    const res = await createUserWithList('apple', '1111');
    expect(res.username).toBe('apple');
  });

  it('should fail to create a user that has a list', async () => {
    expect.hasAssertions();
    try {
      await createUserWithList('apple', '1111');
    } catch (error) {
      expect(error.name).toBe('ResponseError');
      expect(error instanceof ResponseError).toBeTruthy();
    }
  });

  it('should create a user without a list', async () => {
    await User.post('applewithoutlist');
    const res = await createUserWithList('applewithoutlist', '1111');
    expect(res.username).toBe('applewithoutlist');
  });

  it('should fail to create a user without a pin', async () => {
    expect.hasAssertions();
    try {
      await createUserWithList('appletest', '');
    } catch (error) {
      expect(error.name).toBe('ResponseError');
      expect(error instanceof ResponseError).toBeTruthy();
    }
  });
});
