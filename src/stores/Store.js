export default class Store {
  constructor() {
    this.subscribers = [];
  }

  subscribe(component) {
    if (this.subscribers.find((e) => e === component)) return;
    this.subscribers.push(component);
  }

  unsubscribe(component) {
    const i = this.subscribers.findIndex((e) => e === component);
    this.subscribers.splice(i, 1);
  }
}
