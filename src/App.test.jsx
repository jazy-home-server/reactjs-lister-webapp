import React from 'react';
import { cleanup, render } from '@testing-library/react';
import App from 'App';

describe('the main app', () => {
  it('fake render', () => {
    const { getByText } = render(<div>hello</div>);
    const linkElement = getByText(/hello/i);
    expect(linkElement).toBeInTheDocument();
  });

  it('fake test', () => {
    expect(true).toBeTruthy();
  });

  it('should render the main landing page', () => {
    const { getByText } = render(<App />);
    const header = getByText(/Main Lister Landing page/i);
    expect(header).toBeInTheDocument();
  });

  it('should have local dev on body', () => {
    // WARNING app sets a body class that persists between tests
    // Reset body classes
    document.body.className = '';
    render(<App appEnv="local" />);
    expect(document.body.classList.contains('LOCALDEV')).toBeTruthy();
  });

  it('should not have local dev on body in prod', () => {
    // WARNING app sets a body class that persists between tests
    // Reset body classes
    document.body.className = '';
    render(<App appEnv="production" />);
    expect(document.body.classList.contains('LOCALDEV')).toBeFalsy();
  });
});
