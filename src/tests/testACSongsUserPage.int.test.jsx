import React from 'react';
import { render, fireEvent, waitFor, screen } from '@testing-library/react';
import App from 'App';
import { Redirect } from 'react-router-dom';
import ListSong from 'api/ac/listsong';

/**
 * This testing module is very attached to the list, listsong, and user
 * data within the API mocks.
 */

jest.mock('api/ac/list');
jest.mock('api/ac/listsong');
jest.mock('api/ac/user');

const testListSongs = [
  {
    id: 4,
    list_id: 2,
    song_id: 3,
    checked: 1,
    song: {
      id: 4,
      name: 'user_page_test_song',
      img_url: 'user.page.test',
    },
  },
];

beforeAll(() => {
  // Duplicate testListSongs
  testListSongs.forEach((ls) => {
    ListSong.listSongs.push({ ...ls });
  });
});

async function renderUserPage(state) {
  const { getByText } = render(
    <App>
      <Redirect to={{ pathname: '/ac/songs/user/applelover', state }} />
    </App>
  );
}

describe('user page', () => {
  it('should render a loading panel', async () => {
    await renderUserPage();
    expect(screen.getByTestId('page_loading')).toBeInTheDocument();
  });

  it('should render the users name', async () => {
    await renderUserPage();
    await waitFor(() => {
      expect(screen.getByText(/applelover/i)).toBeInTheDocument();
    });
  });

  it('should render the users list songs', async () => {
    await renderUserPage();
    await waitFor(() => {
      const song = screen.getByText(/apple3/i);
      expect(song).toBeInTheDocument();
      expect(song.parentElement.classList.contains('song--checked')).toBeTruthy();
      expect(screen.getByText(/apple2/i)).toBeInTheDocument();
      expect(screen.getByText(/apple1/i)).toBeInTheDocument();
    });
  });

  it('should render the users list songs', async () => {
    await renderUserPage();
    await waitFor(() => {
      expect(screen.getByText(/apple1/i)).toBeInTheDocument();
    });

    const song = screen.getByText(/apple3/i);
    expect(song).toBeInTheDocument();
    expect(song.parentElement.classList.contains('song--checked')).toBeTruthy();
    expect(screen.getByText(/apple2/i)).toBeInTheDocument();
  });

  it('should not change song state with no pin', async () => {
    await renderUserPage();
    await waitFor(() => {
      const pin = screen.getByPlaceholderText(/pin/i);
      expect(pin).toBeInTheDocument();
    });

    const song = screen.getByText(/user_page_test_song/i);
    expect(song).toBeInTheDocument();
    expect(song.parentElement.classList.contains('song--checked')).toBeTruthy();
    fireEvent.click(song);
    expect(song.parentElement.classList.contains('song--checked')).toBeTruthy();
  });

  it('should not change song state with incorrect pin', async () => {
    await renderUserPage();
    const songFocus = jest.fn();
    await waitFor(() => {
      const pin = screen.getByPlaceholderText(/pin/i);
      expect(pin).toBeInTheDocument();
      pin.onfocus = songFocus;
      fireEvent.change(pin, { target: { value: '1235' } });
    });

    const song = screen.getByText(/user_page_test_song/i);
    expect(song).toBeInTheDocument();
    expect(song.parentElement.classList.contains('song--checked')).toBeTruthy();
    fireEvent.click(song);

    await waitFor(() => {
      expect(screen.getByText(/unauthorized/i)).toBeInTheDocument();
      expect(song.parentElement.classList.contains('song--checked')).toBeTruthy();
      expect(songFocus).toHaveBeenCalled();
    });
  });

  it('should change song state with correct pin', async () => {
    await renderUserPage();
    const songFocus = jest.fn();

    await waitFor(() => {
      const pin = screen.getByPlaceholderText(/pin/i);
      expect(pin).toBeInTheDocument();
      pin.onfocus = songFocus;
      fireEvent.change(pin, { target: { value: '1234' } });
    });

    const song = screen.getByText(/user_page_test_song/i);
    expect(song).toBeInTheDocument();
    expect(song.parentElement.classList.contains('song--checked')).toBeTruthy();

    fireEvent.click(song);

    await waitFor(() => {
      expect(song.parentElement.classList.contains('song--checked')).toBeFalsy();
    });

    fireEvent.click(song);

    await waitFor(() => {
      expect(song.parentElement.classList.contains('song--checked')).toBeTruthy();
    });
  });

  it('should change pin state with correct pin', async () => {
    await renderUserPage();
    let pin = null;

    await waitFor(() => {
      pin = screen.getByPlaceholderText(/pin/i);
      expect(pin).toBeInTheDocument();
    });

    fireEvent.change(pin, { target: { value: '1234' } });
    fireEvent.submit(pin);

    const regex = /pinformunlocked/i;
    await waitFor(() => {
      expect(regex.test(pin.parentElement.className)).toBeTruthy();
    });
  });

  it('should not change pin state with incorrect pin', async () => {
    await renderUserPage();
    let pin = null;

    await waitFor(() => {
      pin = screen.getByPlaceholderText(/pin/i);
      expect(pin).toBeInTheDocument();
    });

    fireEvent.change(pin, { target: { value: '1235' } });
    fireEvent.submit(pin);

    const regex = /pinformunlocked/i;
    await waitFor(() => {
      expect(regex.test(pin.parentElement.className)).toBeFalsy();
    });
  });
});
