import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import ListSong from './ListSong';

const listSong = {
  id: 2,
  list_id: 2,
  song_id: 2,
  checked: false,
  song: {
    id: 2,
    name: 'apple2',
    img_url: 'test2.example.com',
  },
};

describe('list song component', () => {
  it('should display song data', () => {
    const handleClick = jest.fn((e, id) => expect(id).toBe(listSong.id));
    const { getByTestId, getByText } = render(
      <ListSong
        songData={listSong.song}
        checked={listSong.checked}
        onClick={handleClick}
        data-testid={2}
      />
    );
    expect(getByTestId('2')).toBeInTheDocument();
    expect(getByText(listSong.song.name)).toBeInTheDocument();
  });

  it('should callback on click', () => {
    const handleClick = jest.fn();
    const { getByRole } = render(
      <ListSong
        songData={listSong.song}
        checked={listSong.checked}
        onClick={handleClick}
        data-testid={2}
      />
    );
    const button = getByRole('button');
    expect(button).toBeInTheDocument();
    fireEvent.click(button);
    expect(handleClick).toHaveBeenCalled();
  });

  it('should show image loading', () => {
    const handleClick = jest.fn();
    const { getByTestId } = render(
      <ListSong
        songData={listSong.song}
        checked={listSong.checked}
        onClick={handleClick}
        data-testid={2}
      />
    );
    expect(getByTestId('image_loading')).toBeInTheDocument();
  });
});
