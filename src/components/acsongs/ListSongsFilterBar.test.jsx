import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import ListSongsFilterBar from './ListSongsFilterBar';

const listSongs = [
  {
    id: 1,
    checked: false,
    song: {
      id: 11,
      name: 'apple',
      img_url: 'noonono',
    },
  },
  {
    id: 2,
    checked: false,
    song: {
      id: 12,
      name: 'middle',
      img_url: 'meow',
    },
  },
  {
    id: 3,
    checked: true,
    song: {
      id: 13,
      name: 'treemilk',
      img_url: 'mooo',
    },
  },
];

describe('filter bar for list song objects', () => {
  it('should call on change', () => {
    const handleChange = jest.fn();
    const { getByRole } = render(
      <ListSongsFilterBar listSongs={listSongs} onChange={handleChange} />
    );
    const input = getByRole('textbox');

    fireEvent.change(input, { target: { value: 'm' } });
    expect(handleChange).toHaveBeenCalledTimes(1);
    fireEvent.change(input, { target: { value: 'ma' } });
    fireEvent.change(input, { target: { value: 'maa' } });
    expect(handleChange).toHaveBeenCalledTimes(3);
  });

  it('should not filter objects when empty', () => {
    const handleChange = jest.fn();
    const { getByRole } = render(
      <ListSongsFilterBar listSongs={listSongs} onChange={handleChange} />
    );
    const input = getByRole('textbox');

    fireEvent.change(input, { target: { value: 'm' } });
    fireEvent.change(input, { target: { value: '' } });
    expect(handleChange).toHaveBeenCalledWith(listSongs.map((e) => ({ ...e, filtered: false })));
  });

  it('should filter objects', () => {
    const handleChange = jest.fn();
    const { getByRole } = render(
      <ListSongsFilterBar listSongs={listSongs} onChange={handleChange} />
    );
    const input = getByRole('textbox');

    fireEvent.change(input, { target: { value: 'm' } });
    expect(handleChange.mock.calls[0][0][0].filtered).toBeTruthy();
    expect(handleChange.mock.calls[0][0][1].filtered).toBeFalsy();
    expect(handleChange.mock.calls[0][0][2].filtered).toBeFalsy();

    fireEvent.change(input, { target: { value: 'mi' } });
    expect(handleChange.mock.calls[1][0][0].filtered).toBeTruthy();
    expect(handleChange.mock.calls[1][0][1].filtered).toBeFalsy();
    expect(handleChange.mock.calls[1][0][2].filtered).toBeFalsy();

    fireEvent.change(input, { target: { value: 'mil' } });
    expect(handleChange.mock.calls[2][0][0].filtered).toBeTruthy();
    expect(handleChange.mock.calls[2][0][1].filtered).toBeTruthy();
    expect(handleChange.mock.calls[2][0][2].filtered).toBeFalsy();

    fireEvent.change(input, { target: { value: 'treemilk' } });
    expect(handleChange.mock.calls[3][0][0].filtered).toBeTruthy();
    expect(handleChange.mock.calls[3][0][1].filtered).toBeTruthy();
    expect(handleChange.mock.calls[3][0][2].filtered).toBeFalsy();

    fireEvent.change(input, { target: { value: 'apple' } });
    expect(handleChange.mock.calls[4][0][0].filtered).toBeFalsy();
    expect(handleChange.mock.calls[4][0][1].filtered).toBeTruthy();
    expect(handleChange.mock.calls[4][0][2].filtered).toBeTruthy();

    fireEvent.change(input, { target: { value: 'apple' } });
    expect(handleChange.mock.calls[4][0][0].filtered).toBeFalsy();
    expect(handleChange.mock.calls[4][0][1].filtered).toBeTruthy();
    expect(handleChange.mock.calls[4][0][2].filtered).toBeTruthy();
  });
});
