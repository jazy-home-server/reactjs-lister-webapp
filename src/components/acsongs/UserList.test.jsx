import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import UserList from './UserList';

const listSongs = [
  {
    id: 1,
    list_id: 2,
    song_id: 1,
    checked: false,
    song: {
      id: 1,
      name: 'apple',
      img_url: 'test.example.com',
    },
  },
  {
    id: 2,
    list_id: 2,
    song_id: 2,
    checked: false,
    song: {
      id: 2,
      name: 'apple2',
      img_url: 'test2.example.com',
    },
  },
];

describe('user list component', () => {
  it('should display songs', () => {
    const { getByTestId, getAllByRole } = render(<UserList listSongs={listSongs} />);
    expect(getByTestId('1')).toBeInTheDocument();
    expect(getAllByRole('button')).toHaveLength(2);
  });

  it('should fire callback on song click', () => {
    const handleSongClick = jest.fn((e, id) => expect(id).toBe(1));
    const { getByTestId } = render(
      <UserList listSongs={listSongs} songClicked={handleSongClick} />
    );
    const songItem = getByTestId('1');
    fireEvent.click(songItem);
    expect(handleSongClick).toHaveBeenCalledTimes(1);
  });

  it('should filter filtered list songs', () => {
    const handleSongClick = jest.fn((e, id) => expect(id).toBe(1));
    const filteredListSong = listSongs.map((e) => ({ ...e, filtered: true }));
    const { getByTestId } = render(
      <UserList listSongs={filteredListSong} songClicked={handleSongClick} />
    );
    const songItem = getByTestId('1');
    expect(songItem.classList.contains('userList__listSong--filtered')).toBeTruthy();
  });

  it('should not filter unfiltered list songs', () => {
    const handleSongClick = jest.fn((e, id) => expect(id).toBe(1));
    const filteredListSong = listSongs.map((e) => ({ ...e, filtered: false }));
    const { getByTestId } = render(
      <UserList listSongs={filteredListSong} songClicked={handleSongClick} />
    );
    const songItem = getByTestId('1');
    expect(songItem.classList.contains('userList__listSong--filtered')).toBeFalsy();
  });
});
