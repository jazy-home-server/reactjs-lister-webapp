import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import { ThemeContext } from 'themeContext';
import ThemeButton from './ThemeButton';

describe('theme button', () => {
  it('should render', () => {
    const { queryByTestId } = render(<ThemeButton />);
    const linkElement = queryByTestId('button');
    expect(linkElement).toBeInTheDocument();
  });

  it('should toggle the context theme', () => {
    const mockContext = {
      toggleTheme: jest.fn(),
    };
    const { queryByTestId } = render(
      <ThemeContext.Provider value={mockContext}>
        <ThemeButton />
      </ThemeContext.Provider>
    );
    fireEvent(queryByTestId('button'), new MouseEvent('click', { bubbles: true }));
    expect(mockContext.toggleTheme).toHaveBeenCalledTimes(1);
    fireEvent(queryByTestId('button'), new MouseEvent('click', { bubbles: true }));
    expect(mockContext.toggleTheme).toHaveBeenCalledTimes(2);
  });
});
