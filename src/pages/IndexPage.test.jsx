import React from 'react';
import { render } from '@testing-library/react';
import IndexPage from './IndexPage';

test('render main lister index page', () => {
  const { getByText } = render(<IndexPage />);
  const linkElement = getByText(/lister/i);
  expect(linkElement).toBeInTheDocument();
});
