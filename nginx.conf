server {
    listen 80 default_server;
    server_name localhost;

    root /usr/share/nginx/html;
    index index.html index.htm;

    location = /favicon.ico { access_log off; log_not_found off; }
    location = /robots.txt  { access_log off; log_not_found off; }

    add_header X-Frame-Options "SAMEORIGIN";
    add_header Content-Security-Policy "frame-ancestors 'self'";

    set $REPORT_URL "https://api.jazyserver.com/meta/logger/csp-reports";
    set $REPORT_URI "report-uri ${REPORT_URL}";
    set $SCRIPT "script-src 'self'";
    set $SCRIPT "${SCRIPT} *.jazyserver.com";
    set $STYLE "style-src 'self'";
    set $STYLE "${STYLE} fonts.googleapis.com";
    add_header Content-Security-Policy-Report-Only "${REPORT_URI}; ${SCRIPT}; ${STYLE}";

    add_header X-Content-Type-Options "nosniff";

    location ~* \.(?:manifest|appcache|html?|xml|json)$ {
      expires -1;
      # access_log logs/static.log;
    }

    location ~* \.(?:css|js)$ {
      try_files $uri =404;
      expires 1y;
      access_log off;
      add_header Cache-Control "public";
    }

    # Any route containing a file extension (e.g. /devicesfile.js)
    location ~ ^.+\..+$ {
      try_files $uri =404;
    }

    # Any route that doesn't have a file extension (e.g. /devices)
    location / {
        try_files $uri $uri/ /index.html;
    }
}