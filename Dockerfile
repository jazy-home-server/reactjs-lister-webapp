# ReactJS dev, build, and production
FROM node:12 AS dev

RUN mkdir /app
WORKDIR /app

FROM dev AS build

COPY package*.json ./
RUN yarn

COPY . ./
RUN INLINE_RUNTIME_CHUNK=false CI=true yarn build

FROM nginx:alpine
ARG SERVICE_NAME="lister"
LABEL maintainer="jazy@jazyserver.com"
LABEL com.jazyserver.lister=$SERVICE_NAME

COPY nginx.conf /etc/nginx/conf.d/
RUN mv /etc/nginx/conf.d/nginx.conf /etc/nginx/conf.d/default.conf

COPY --from=build /app/build /usr/share/nginx/html
EXPOSE 80

CMD ["nginx", "-g", "daemon off;"]